## Slidify

Starting a new Slidify presentation.

    > library(slidify)
    > author("test")
    > slidify("index.Rmd")
    > library(knitr)
    > browseURL("index.html")

YAML block at top of file:

    ---
    title       : Slidify
    subtitle    : Data meets presentation
    author      : Brian Caffo, Jeffrey Leek, Roger Peng 
    job         : Johns Hopkins Bloomberg School of Public Health
    logo        : bloomberg_shield.png
    framework   : io2012        # {io2012, html5slides, shower, dzslides, ...}
    highlighter : highlight.js  # {highlight.js, prettify, highlight}
    hitheme     : tomorrow      # 
    widgets     : [mathjax, quiz, bootstrap]
    mode        : selfcontained # {standalone, draft}
    ---